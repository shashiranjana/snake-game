import pygame
import random
import sys
import time

pygame.init()
screen = pygame.display.set_mode((500, 500))
pygame.display.set_caption('Snake Game')
white=(255,255,255)
body_rect=[[100,40],[90,40]]

def check_collision(position):
    '''
    check collision for inner body and boundary line
    '''
    if position[0] > 500 or position[0] < 0:
        return 1
    elif position[1] > 500 or position[1] < 0:
        return 1
    for bodyPart in body_rect[1:]:
        if position == bodyPart:
            return 1
    return 0

def re_play():
    keys= pygame.key.get_pressed()
    if keys[pygame.K_SPACE]:
        game_loop()
    elif keys[pygame.K_ESCAPE]:
        pygame.quit()
        sys.exit()

def game_over(total_score):
    '''
    quit game
    '''
    font = pygame.font.SysFont("comicsansms", 30)
    font_in = pygame.font.SysFont("Helvetica", 25)
    score_value = font.render("Score Won:"+str(total_score), True, (0, 128, 0))
    instruction_for_replay = font_in.render("Press SPACE BAR for Replay", True, (48, 80, 197))
    instruction_for_exit = font_in.render("Press ESC for Exit", True, (48, 80, 197))
    screen.fill(white)
    screen.blit(score_value,(150, 150))
    screen.blit(instruction_for_replay,(100, 200))
    screen.blit(instruction_for_exit,(150, 240))
    re_play()

def game_loop():
    '''
    main game loop
    '''
    vel=5
    y=40
    x=100
    run =True
    temp_food=[300,300]
    temp_position="RIGHT"
    while run:
        pygame.time.delay(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
        keys= pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            x-=vel
            temp_position="LEFT"
        if keys[pygame.K_RIGHT]:
            x+=vel
            temp_position="RIGHT"
        if keys[pygame.K_UP]:
            y-=vel
            temp_position="UP"
        if keys[pygame.K_DOWN]:
            y+=vel
            temp_position="DOWN"
        # rotate rect to his respective direction
        if keys.count(1)==0:
            if temp_position=="RIGHT":
                x+=vel
            if temp_position=="LEFT":
                x-=vel
            if temp_position=="UP":
                y-=vel
            if temp_position=="DOWN":
                y+=vel

        screen.fill((0,0,0))
        pygame.draw.rect(screen,(255,255,255),(temp_food[0], temp_food[1], 10, 10))
        for pos in body_rect:
            pygame.draw.rect(screen, pygame.Color(0, 225, 0), pygame.Rect(pos[0], pos[1], 10, 10))
        current_pos=[x,y]
        result_collision=check_collision(current_pos)
        if result_collision == 1:
            total_score=len(body_rect)-2
            game_over(total_score)

        if current_pos not in body_rect:
            body_rect.insert(0,current_pos)
            if current_pos== temp_food:
                random_pos = [random.randrange(1, 50) * 10, random.randrange(1, 50) * 10]
                temp_food=random_pos
            else:
                body_rect.pop()
        pygame.display.update()
        pygame.display.flip()

game_loop()
